#ifndef SNAKE_H
#define SNAKE_H

#include <vector>
#include <iostream>
#include "func.h"

using namespace std;

class Snake {
public :
	vector<float> x;
	vector<float> y;
	vector<float> z;
	float xSize;
	float ySize;
	float zSize;
	int dir;
	func* F;

	Snake (float X, float Y, float Z, float XSize, float YSize, float ZSize, int Dir);
	~Snake ();

	bool MoveSnake ();
	void DrawSnake (); 
	void AddBlock (float X, float Y, float Z);
private:
	bool Colide ();
};

#endif
