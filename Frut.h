#pragma once
#include "func.h"
#include "Snake.h"

class Frut
{
public:
	float x;
	float y;
	float size;
	func *F;
	Snake *snake;

	Frut(float X, float Y, func* f, Snake* s);
	void NewPos ();
	void Colide (float xOb, float yOb);
	void Draw ();
};
