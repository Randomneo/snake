#include <iostream>
#include <GL/glut.h>
#include <math.h>
#include <vector>
#include "Snake.h"
#include "Frut.h"

using namespace std;

float rot = 0;

Snake* snake = new Snake(3, 3, 0, 1, 1, 1, 0);
func* f = snake->F;
Frut* frut = new Frut(5, 5, snake->F, snake);

int moveRot = 0;
float width = 1366, height = 756;

void Display ()
{
	glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glEnable ( GL_DEPTH_TEST );

	glLoadIdentity ();
	glTranslatef ( 0, 0, -30 );
	
	glRotatef (-65, 1, 0, 0);
	glRotatef (rot, 0, 0, 1);

	glTranslatef (-snake->xSize/2, -snake->ySize/2, 0);
	
	snake->DrawSnake();

	glTranslatef (-snake->x[0], -snake->y[0], snake->z[0]);

	float color1[3] = {0, 0, 0}, color2[3] = {0, 0.5, 0.5};
	f->DrawNet (0, 0, 5, color1, color2, 20);

	frut->Draw();

	glFlush ();
	glutSwapBuffers ();
}

void Timer ( int = 0 )
{

	rot += f->AddToRot ( snake->dir, rot, 5 );
	rot = f->r360( rot );
	 
	frut->Colide (snake->x[0], snake->y[0]);
	if (snake->MoveSnake ())
	{
		exit(0);
	}
	
	Display ();
	glutTimerFunc ( 50, Timer, 0 );
}

void SKey (int key, int a, int b)
{
	int move = 0;
	switch (key)
	{
		case GLUT_KEY_UP : move = 1; break;
		case GLUT_KEY_DOWN : move = -1; break;
		case GLUT_KEY_LEFT : snake->dir--; break;
		case GLUT_KEY_RIGHT : snake->dir++; break;
	}

	snake->dir = (snake->dir+4)%4;
}

int main (int argc, char** argv)
{
	glutInit ( &argc, argv );
	glutInitWindowSize ( width, height);
	glutInitWindowPosition ( 0, 0 );
	glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	glutCreateWindow ( "Test1" );

	glClearColor ( 0, 0, 1, 1 );
	glMatrixMode ( GL_PROJECTION );
	glLoadIdentity ();
	gluPerspective ( 50, width/height, 1, 10000);
	glMatrixMode ( GL_MODELVIEW );

	glutDisplayFunc ( Display );
	glutTimerFunc ( 50, Timer, 0 );
	glutSpecialFunc (SKey);

	glutMainLoop ();

	return 0;
}
