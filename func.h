#pragma onse
#ifndef FUNC_H
#define FUNC_H

class func
{
public :
	func ();
	void DrawCube (float x, float y, float z, float xl, float yl, float zl);
	void DrawNet (int x, int y, int step, float color1[3], float color2[3], int coll);
	float r360 (float a);
	float AddToRot (float dir, float rot, float rotSpeed);
};

#endif
