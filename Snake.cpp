#include <iostream>
#include <vector>
#include <GL/glut.h>
#include "Snake.h"

using namespace std;

Snake::Snake (float X, float Y, float Z, float XSize,
							float YSize, float ZSize, int Dir)
{
	for (int i = 0; i < 3; ++i)
	{
		x.push_back (X);
		y.push_back (Y);
		z.push_back (Z);
	}
	
	F = new func();

	xSize = XSize;
	ySize = YSize;
	zSize = ZSize;

	dir = Dir;
}

void Snake::DrawSnake ()
{
	glColor3f ( 1, 1, 0 );
	F->DrawCube (0, 0, 0, xSize, ySize, zSize);

	glColor3f ( 0, 1, 0 );
	for (int i = 1; i < x.size(); ++i)
		F->DrawCube ( x[i] - x[0], y[i] - y[0], z[0], xSize, ySize, zSize);
}

bool Snake::Colide ()
{
	for (int i = 1; i < x.size(); ++i)
	{
		if (x[0] == x[i] && y[0] == y[i])
		{
			return true;
		}
	}
	return false;
}
bool Snake::MoveSnake ()
{
	for (int i = x.size()-1; i > 0; --i)
	{
		x[i] = x[i-1];
		y[i] = y[i-1];
	}
	
	float movespeed = 1;
	switch (dir)
	{
		case 0 : y[0] += movespeed; break;
		case 1 : x[0] += movespeed; break;
		case 2 : y[0] -= movespeed; break;
		case 3 : x[0] -= movespeed; break;
	}

	if (Colide())
	{
		return true;
	}
	if (x[0] < 0 || y[0] < 0 || x[0] > 99 || y[0] > 99)
	{
		return true;
	}
	
	return false;
}
void Snake::AddBlock (float X, float Y, float Z)
{
	x.push_back (X);
	y.push_back (Y);
	z.push_back (Z);
}
