#include <GL/glut.h>
#include "func.h"

func::func ()
{
}

void func::DrawCube (float x, float y, float z, float xl, float yl, float zl)
{
	glBegin ( GL_QUADS );

	glVertex3f (x, y, z);
	glVertex3f (x + xl, y, z);
	glVertex3f (x + xl, y + yl, z);
	glVertex3f (x, y + yl, z);

	glVertex3f (x, y, z);
	glVertex3f (x + xl, y, z);
	glVertex3f (x + xl, y, z + zl);
	glVertex3f (x, y, z + zl);

	glVertex3f (x, y, z);
	glVertex3f (x, y + yl, z);
	glVertex3f (x, y + yl, z + zl);
	glVertex3f (x, y, z + zl);

	glVertex3f (x + xl, y + yl, z + zl);
	glVertex3f (x, y + yl, z + zl);
	glVertex3f (x, y, z + zl);
	glVertex3f (x + xl, y, z + zl);
	
	glVertex3f (x + xl, y + yl, z + zl);
	glVertex3f (x, y + yl, z + zl);
	glVertex3f (x, y + yl, z);
	glVertex3f (x + xl, y + yl, z);	

	glVertex3f (x + xl, y + yl, z + zl);
	glVertex3f (x + xl, y, z + zl);
	glVertex3f (x + xl, y, z);
	glVertex3f (x + xl, y + yl, z);

	glEnd ();
}
void func::DrawNet (int x, int y, int step, float color1[3], float color2[3], int coll)
{
	for (int i = x; i < step * coll + x; i += step)
	{
		for (int j = y; j < step * coll + y; j += step)
		{
			if ((i + j) % 2 == 0)
				glColor3f (color1[0], color1[1], color1[2]);
			else
				glColor3f (color2[0], color2[1], color2[2]);
			
			glBegin (GL_QUADS);
			
			glVertex3f (i, j, 0);
			glVertex3f (i + step, j, 0);
			glVertex3f (i + step, j + step, 0);
			glVertex3f (i, j + step, 0);
			
			glEnd ();
		}
	}
}

float func::r360 (float a)
{
	if (a >= 360)
		a -= 360;
	if (a < 0)
		a += 360;

	return a;
}

float func::AddToRot (float dir, float rot, float rotSpeed)
{
	dir *= 90;
        if (rot != dir)
        {
                if (rot > dir)
		{
			if (rot - dir < 360 - rot + dir)
				return -rotSpeed;
			else if (rot - dir > 360 - rot + dir)
				return rotSpeed;
		}
		else if (rot < dir)
		{
			if (r360(rot - dir) < -rot + dir)
				return -rotSpeed;
			else if (r360(rot - dir) > -rot + dir)
				return rotSpeed;
                }
                return rotSpeed;
        }
		
	return 0;
}
