#include "Frut.h"
#include <GL/glut.h>
#include <time.h>
#include "Snake.h"

Frut::Frut (float X, float Y, func *f, Snake* s)
{
	x = X;
	y = Y;
	size = 3;
	F = f;
	snake = s;
}

void Frut::NewPos ()
{
	x = rand() % (100 - (int)size);
	y = rand() % (100 - (int)size);
}

void Frut::Colide (float xOb, float yOb)
{
	if (xOb >= x && xOb < x + size && yOb >= y && yOb < y + size)
	{
		NewPos ();
		snake->AddBlock (0, 0, 0);
	}
}

void Frut::Draw ()
{
	glColor3f (1, 0, 0);
	F->DrawCube (x, y, 0, size, size, size);
}
